
// from http://stackoverflow.com/questions/9359913/writing-to-a-existing-xls-file-using-poi
//import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class PoiWriteExcelFile {

	private static final String PATH_FILE;
	private static final Log LOG;

	static {
		LOG = LogFactory.getLog(PoiWriteExcelFile.class);
		PATH_FILE = "/home/cesar/Documentos/MedicionesRubickdeleteme.xls";
	}

	public void methodOne() {

		LOG.debug("Into method one!");

		final Workbook wb = new HSSFWorkbook();
		final Font f = wb.createFont();
		f.setBoldweight(Font.BOLDWEIGHT_BOLD);
		final CellStyle cs = wb.createCellStyle();
		cs.setFont(f);

		final CreationHelper createHelper = wb.getCreationHelper();
		final Sheet sheet = wb.createSheet("First Sheet");
		final Row row = sheet.createRow((short) 0);

		Cell c;

		c = row.createCell(0);
		c.setCellStyle(cs);
		c.setCellValue(createHelper.createRichTextString("First Column"));

		c = row.createCell(1);
		c.setCellStyle(cs);
		c.setCellValue(createHelper.createRichTextString("Second Column"));

		c = row.createCell(2);
		c.setCellStyle(cs);
		c.setCellValue(createHelper.createRichTextString("Third Column"));

		writeOutput(wb);

		LOG.debug("Out of method one!");

	}

	private void writeOutput(final Workbook wb) {
		try {
			final FileOutputStream fileOut = new FileOutputStream(PATH_FILE);
			wb.write(fileOut);
			fileOut.close();
		} catch (FileNotFoundException e) {
			LOG.error("", e);
		} catch (IOException e) {
			LOG.error("", e);
		}
	}

	private Workbook openFile() {
		try {
			final InputStream inp = new FileInputStream(PATH_FILE);
			final Workbook wb = WorkbookFactory.create(inp);
			return wb;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			LOG.error("", e);
		}
		return null;
	}

	public void methodTwo() {

		LOG.debug("Into method two!");

		final Workbook wb = openFile();// WorkbookFactory.create(inp);
		final Sheet sheet = wb.getSheetAt(0);
		final Row row = sheet.createRow((short) (sheet.getLastRowNum() + 1));

		final CreationHelper createHelper = wb.getCreationHelper();

		Cell c = row.createCell(0);
		c.setCellValue(createHelper.createRichTextString("First Row First value"));

		c = row.createCell(1);
		c.setCellValue(createHelper.createRichTextString("First Row Second value"));

		c = row.createCell(2);
		c.setCellValue(createHelper.createRichTextString("First Row Third value"));

		writeOutput(wb);

		LOG.debug("Out of method two!");
	}

	public void methodThree() {

		LOG.debug("Into method three!");
		final Workbook wb = openFile();
		final Sheet sheet = wb.getSheetAt(0);
		final Row row = sheet.createRow((short) (sheet.getLastRowNum() + 1));

		Cell c;
		CreationHelper createHelper = wb.getCreationHelper();

		c = row.createCell(0);
		c.setCellValue(createHelper.createRichTextString("Second Row First value"));

		c = row.createCell(1);
		c.setCellValue(createHelper.createRichTextString("Second Row Second value"));

		c = row.createCell(2);
		c.setCellValue(createHelper.createRichTextString("Second Row Third value"));

		writeOutput(wb);

		LOG.debug("Out of method three!");

	}

	public void methodFour() {

		LOG.debug("Into method four!");
		// File file = new File(PATH_FILE);
		// file.deleteOnExit();
		LOG.debug("Out of method four!");

	}

}
